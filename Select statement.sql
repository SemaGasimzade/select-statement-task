--select statement 
select first_name,last_name,count(department_id) from employees 
where count(department_id)>10
group by first_name,last_name 
HAVING salary>600 
order by first_name fetch first 10 rows only
--single row functions
select upper(first_name),lower(last_name),length(email) from employees where length(email)>4;
select months_between ('18-03-1969', '18-01-1969')
as months from dual;
select round(product_weight,2),trunc(product_height,1) from sales_data;
--initcap,MOD, SUBSTR, INSTR bilmirem
--aggregate functions
select count(department_id),department_id from employees  group by department_id;
select min(salary),max(salary),avg(salary) from employees;
--median'i bilmirem
